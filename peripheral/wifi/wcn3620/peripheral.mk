#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

WLAN_BSP_SRC = hardware/bsp/qcom/peripheral/wifi/wcn3620

WLAN_BIN_SRC_PATH = hardware/bsp/kernel/qcom/qcom-msm-3.10/drivers/staging/prima/firmware_bin/

ifeq ($(TARGET_DEVICE),apq8076_64)
WLAN_BIN_SRC = device/qcom/apq8076/firmware/wlan/prima/
else
WLAN_BIN_SRC = device/qcom/$(TARGET_DEVICE)/firmware/wlan/prima/
endif


FIRMWARE_DST = system/vendor/firmware

# WiFi driver -- loaded by drivers/staging/prima.
WLAN_FIRMWARE_DST = $(FIRMWARE_DST)/wlan/prima

PRIMA_FILES = \
  WCNSS_qcom_cfg.ini \
  WCNSS_qcom_wlan_nv.bin

PRIMA_FILES_PATH = \
  WCNSS_cfg.dat

PRIMA_COPY_FILES += \
  $(join $(patsubst %, $(WLAN_BIN_SRC)/%, $(PRIMA_FILES)), $(patsubst %, :$(WLAN_FIRMWARE_DST)/%, $(PRIMA_FILES)))

PRIMA_COPY_FILES += \
  $(join $(patsubst %, $(WLAN_BIN_SRC_PATH)/%, $(PRIMA_FILES_PATH)), $(patsubst %, :$(WLAN_FIRMWARE_DST)/%, $(PRIMA_FILES_PATH)))

PRODUCT_COPY_FILES += $(PRIMA_COPY_FILES)

WIFI_DRIVER_HAL_MODULE := wifi_driver.$(soc_name)
WIFI_DRIVER_HAL_PERIPHERAL := wcn3620

BOARD_SEPOLICY_DIRS += $(WLAN_BSP_SRC)/sepolicy
