#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := hardware/bsp/qcom/soc/msm8952

BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.console=ttyHSL0 androidboot.hardware=msm8952 msm_rtb.filter=0x237 ehci-hcd.park=3 androidboot.bootdevice=7824900.sdhci lpm_levels.sleep_disabled=1 androidboot.selinux=enforcing

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.msm8952.rc:root/init.msm8952.rc \
	$(LOCAL_PATH)/ueventd.msm8952.rc:root/ueventd.msm8952.rc \
	system/core/rootdir/init.usb.rc:root/init.usb.rc \
	system/core/rootdir/ueventd.rc:root/ueventd.rc \

BOARD_SEPOLICY_DIRS += \
	$(LOCAL_PATH)/sepolicy \
	$(LOCAL_PATH)/prebuilts/sepolicy \

# Set up the local kernel.
TARGET_KERNEL_SRC := hardware/bsp/kernel/qcom/qcom-msm-3.10

# using 8916 config for 32-bit is intentional.
ifeq ($(TARGET_DEVICE),apq8076_64)
  ifeq ($(TARGET_BUILD_VARIANT),eng)
    TARGET_KERNEL_DEFCONFIG := msm_defconfig
  else
    TARGET_KERNEL_DEFCONFIG := msm-perf_defconfig
  endif
else
  ifeq ($(TARGET_BUILD_VARIANT),eng)
    TARGET_KERNEL_DEFCONFIG := msm8916_defconfig
  else
    TARGET_KERNEL_DEFCONFIG := msm8916-perf_defconfig
  endif
endif

# Add additional kernel configs.
ifeq ($(TARGET_BUILD_VARIANT),eng)
    $(call add_kernel_configs, $(realpath $(LOCAL_PATH)/soc-eng.kconf))
else
    $(call add_kernel_configs, $(realpath $(LOCAL_PATH)/soc.kconf))
endif

# Default Keystore HAL.
DEVICE_PACKAGES += \
	keystore.default

# Include Bool Control HAL.
DEVICE_PACKAGES += \
	bootctrl.$(soc_name)

# Audio feature flags.
BOARD_USES_ALSA_AUDIO := true
AUDIO_FEATURE_ENABLED_EXTN_RESAMPLER := true
AUDIO_FEATURE_ENABLED_HFP := true
AUDIO_FEATURE_ENABLED_MULTI_VOICE_SESSIONS := true
AUDIO_FEATURE_ENABLED_VOICE_CONCURRENCY := true
AUDIO_FEATURE_ENABLED_WFD_CONCURRENCY := true
BOARD_USES_SRS_TRUEMEDIA := true
BOARD_SUPPORTS_SOUND_TRIGGER := true
AUDIO_FEATURE_ENABLED_DS2_DOLBY_DAP := true
AUDIO_FEATURE_ENABLED_ACDB_LICENSE := true
DOLBY_DAP_HW_QDSP_HAL_API := true
DOLBY_UDC_MULTICHANNEL_PCM_OFFLOAD := false
MM_AUDIO_ENABLED_FTM := true
MM_AUDIO_ENABLED_SAFX := true
TARGET_USES_QCOM_MM_AUDIO := true
AUDIO_FEATURE_ENABLED_QTI_EXTN := true
# Include Audio HAL implementation.
DEVICE_PACKAGES += \
	audio.primary.$(soc_name)

# Install device-specific audio policy, audio effects config, media codecs and mixer path files.
PRODUCT_COPY_FILES += \
	device/qcom/apq8076/audio_policy.conf:system/etc/audio_policy.conf \
	device/qcom/apq8076/audio_effects.conf:system/etc/audio_effects.conf \
	device/qcom/apq8076/audio_io_policy.conf:system/vendor/etc/audio_io_policy.conf \
	device/qcom/apq8076/audio_platform_info_extcodec.xml:system/etc/audio_platform_info_extcodec.xml \
	device/qcom/apq8076/mixer_paths.xml:system/etc/mixer_paths.xml \
	device/qcom/apq8076/mixer_paths_wcd9306.xml:system/etc/mixer_paths_wcd9306.xml \
	device/qcom/apq8076/mixer_paths_wcd9326.xml:system/etc/mixer_paths_wcd9326.xml \
	device/qcom/apq8076/mixer_paths_wcd9330.xml:system/etc/mixer_paths_wcd9330.xml \
	device/qcom/apq8076/mixer_paths_wcd9335.xml:system/etc/mixer_paths_wcd9335.xml \
	device/qcom/apq8076/sound_trigger_mixer_paths.xml:system/etc/sound_trigger_mixer_paths.xml \
	device/qcom/apq8076/sound_trigger_mixer_paths_wcd9306.xml:system/etc/sound_trigger_mixer_paths_wcd9306.xml \
	device/qcom/apq8076/sound_trigger_mixer_paths_wcd9330.xml:system/etc/sound_trigger_mixer_paths_wcd9330.xml \
	device/qcom/apq8076/sound_trigger_mixer_paths_wcd9335.xml:system/etc/sound_trigger_mixer_paths_wcd9335.xml \
	device/qcom/apq8076/sound_trigger_platform_info.xml:system/etc/sound_trigger_platform_info.xml \
	device/qcom/apq8076/media/media_codecs_performance_8956_v1.xml:system/etc/media_codecs_performance.xml \
	device/qcom/apq8076/media/media_codecs_8956_v1.xml:system/etc/media_codecs.xml

# The scripts configure and enable USB features on MSM devices at startup.
PRODUCT_COPY_FILES += \
        device/qcom/common/rootdir/etc/init.qcom.usb.rc:root/init.qcom.usb.rc \
        device/qcom/common/rootdir/etc/init.qcom.usb.sh:root/init.qcom.usb.sh \

# Include prebuilts to detect audio devices.
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/prebuilts/audio.rc:system/etc/init/audio.rc \

# Include prebuilts to support keymaster v1.
PRODUCT_COPY_FILES += \
        $(LOCAL_PATH)/prebuilts/qseecom.rc:system/etc/init/qseecom.rc \

# Camera HAL.
DEVICE_PACKAGES += \
       camera.$(soc_name) \
       libskia \
       libgif \
       libsfntly \
       libwebp-decode \
       libft2

# Gralloc HAL.
DEVICE_PACKAGES += \
       gralloc.$(soc_name) \
       gralloc.default \
       libmemalloc

# Display utils library.
DEVICE_PACKAGES += \
       libqdutils

# Media libraries.
DEVICE_PACKAGES += \
       libOmxCore \
       libmm-omxcore \
       libstagefrighthw \
       libOmxVdec \
       libOmxVenc \
       libc2dcolorconvert

#Install pre built BSP if present.
PRODUCT_LIBRARY_PATH := $(TOP)/vendor/bsp/qcom/device/$(PRODUCT_DEVICE)/board_support_package
SYSTEM_BINS    :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/bin/*)
SYSTEM_LIBS    :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/lib/*.so)
SYSTEM_LIBS_64 :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/lib64/*.so)
ETC_FIRMWARE   :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/etc/firmware/*)

#System bins.
PRODUCT_COPY_FILES += $(foreach file,$(SYSTEM_BINS),\
        $(file):/system/bin/$(notdir $(file))) \

#system libs.
PRODUCT_COPY_FILES += $(foreach file,$(SYSTEM_LIBS),\
        $(file):/system/lib/$(notdir $(file))) \

PRODUCT_COPY_FILES += $(foreach file,$(SYSTEM_LIBS_64),\
        $(file):/system/lib64/$(notdir $(file))) \

#etc firmware.
PRODUCT_COPY_FILES += $(foreach file,$(ETC_FIRMWARE),\
        $(file):/system/etc/firmware/$(notdir $(file))) \

PRODUCT_COPY_FILES += \
        $(LOCAL_PATH)/init.firewall-concam-setup.sh:system/etc/init.firewall-concam-setup.sh

# Test product artifact
ifeq ($(BOARD_PRODUCT_TEST_ARTIFACT),true)
DEVICE_PACKAGES += \
       libminijail_test \
       brillo_camera_client \
       audio_hal_playback_test \
       audio_hal_record_test \
       brillo_audio_test \
       hal_play \
       hal_multi_rec \
       hal_play_kpi \
       hal_rec_kpi \
       tinymix \
       tinyplay \
       crash_reporter_tests \
       libweave_test \
       metrics_collector_tests \
       metricsd_tests \
       apmanager_test \
       libnativepower_tests \
       nativepowerman_tests \
       shill_test \
       weaved_test
endif

DEVICE_PACKAGES += \
       reboot-daemon

PRODUCT_COPY_FILES += \
        device/qcom/common/sec_config:system/etc/sec_config

# Script to setup symbolic links to firmware directory
PRODUCT_COPY_FILES += \
        device/qcom/apq8076/init.qcom.modem_links.sh:system/etc/init.qcom.modem_links.sh
